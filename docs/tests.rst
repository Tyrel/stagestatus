Tests
=====


Setup
-----

First install the testing requirements with ``pip install -r requirements/testing.txt``

This will install the fake model factory generator, and test runners.


Running
-------

To run tests, simply use ``tox`` this will instal all deps, and run the ``manage.py test`` command, invoke Nose and pickup all the tests.

To start the auto testing, just run ``sniffer`` from the root project directory, this will watch all files in ``stagestatus/`` for changes, and if any ``.py`` files are changed it will re-run the tests with ``tox``.

