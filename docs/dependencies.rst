Dependencies
============


Python
~~~~~~


Django Nose
-----------

https://django-nose.readthedocs.io/en/latest/usage.html

django-nose provides all the goodness of nose in your Django tests.

Sniffer
-------

https://github.com/jeffh/sniffer/

sniffer is a autotest tool for Python using the nosetest library.
