from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from stagestatus.views import redirect_machines

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'machines/', include('machines.urls')),
    url(r'', redirect_machines),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
