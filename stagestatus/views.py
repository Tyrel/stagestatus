from django.http import HttpResponseRedirect
from django.shortcuts import reverse


def redirect_machines(request):
    return HttpResponseRedirect(reverse('machines-list'))
