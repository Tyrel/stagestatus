from machines.views import MachineList, MachineDetail
from django.conf.urls import url, include
from machines.api.routers import router

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^(?P<pk>\w+)/$', MachineDetail.as_view(), name='machine-detail'),
    url(r'^$', MachineList.as_view(), name='machines-list'),
]
