from __future__ import print_function

from django.test import TransactionTestCase


# Models
from django.contrib.auth.models import User


from machines.tests.utils import make_machine
from machines.models import Machine


class ModelsTest(TransactionTestCase):
    cleans_up_after_itself = True

    def setUp(self):
        self.user = User.objects.create(username='tyrel')
        self.hostname = 'test'
        _m, _ms, _mr = make_machine(self.hostname, self.user)
        self.machine = _m
        self.machine_status = _ms
        self.machine_reservation = _mr

    def test_assertions(self):
        """ General assertions to test models """
        self.assertEqual(self.machine_status.machine, self.machine)
        self.assertEqual(self.machine_reservation.machine, self.machine)
        self.assertEqual(self.machine_status.machine,
                         self.machine_reservation.machine)

    def test_is_reserved(self):
        self.assertTrue(self.machine.is_reserved())

        self.machine_reservation.delete()

        # refresh_from_db isn't working for some reason.
        machine = Machine.objects.get(hostname=self.hostname)

        self.assertFalse(machine.is_reserved())
