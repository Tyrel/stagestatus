from machines.models import Machine, MachineStatus, MachineReservation


def make_machine(machine_name, user):
    """
        Sets up a test machine, status, and a reservation
    """
    machine = Machine.objects.create(
        hostname=machine_name, db_server=machine_name)
    machine_status = MachineStatus.objects.create(
        machine=machine,
        git_branch='master',
        git_version='master-tag')
    machine_reservation = MachineReservation.objects.create(
        user=user,
        machine=machine
    )
    return machine, machine_status, machine_reservation
