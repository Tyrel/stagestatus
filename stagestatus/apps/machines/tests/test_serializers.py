from __future__ import print_function
import json

from django.test import TransactionTestCase

from rest_framework.renderers import JSONRenderer

# Models
from django.contrib.auth.models import User


from machines.api.serializers import MachineSerializer
from machines.tests.utils import make_machine


class SerializerTest(TransactionTestCase):
    cleans_up_after_itself = True

    def setUp(self):
        self.user = User.objects.create(username='tyrel')
        _m, _ms, _mr = make_machine('test', self.user)
        self.machine = _m
        self.machine_status = _ms
        self.machine_reservation = _mr

    def test_serializer(self):
        """
            Test to make sure the api returns data in proper format,
            not just the hostname and db_server
        """
        machine = MachineSerializer(self.machine)
        machine_json = JSONRenderer().render(machine.data)
        machine_dict = json.loads(machine_json)

        keys = set(machine_dict.keys())
        assumed_keys = set((u'db_server', u'hostname', u'is_reserved',
                            u'reservation', u'status'))
        self.assertEqual(assumed_keys, keys)
