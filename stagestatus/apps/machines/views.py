from django.views.generic import ListView, DetailView
from machines.models import Machine
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


@method_decorator(login_required, name='dispatch')
class MachineList(ListView):
    context_object_name = 'machines'
    queryset = Machine.objects.all()


@method_decorator(login_required, name='dispatch')
class MachineDetail(DetailView):
    context_object_name = 'machine'
    model = Machine
