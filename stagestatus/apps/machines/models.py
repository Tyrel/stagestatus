from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Machine(models.Model):
    """
        A staging machine.
        This is the data that doesn't really change much.
    """
    hostname = models.CharField(
        primary_key=True,
        max_length=64,
        help_text="Server's hostname")

    db_server = models.CharField(
        max_length=256,
        help_text='Database server host')

    def is_reserved(self):
        try:
            return bool(self.reservation)
        except MachineReservation.DoesNotExist:
            return False

    def __unicode__(self):
        return self.hostname


class MachineStatus(models.Model):
    """
        Represents the data that we store on each server.
        This data will change every time a machine is checked out,
        or nightly refreshes, or anything.
    """
    machine = models.OneToOneField('Machine',
                                   related_name='status')

    git_branch = models.CharField(
        default='master',
        max_length=256,
        help_text='Git branch checked out on server.')
    git_version = models.CharField(
        default='master',
        max_length=256,
        help_text='Current tag of the git branch')
    last_db_refresh = models.DateTimeField(
        null=True,
        help_text='Time the last database refresh completed')
    last_updated = models.DateTimeField(
        auto_now=True,
        help_text='The time this was last modified')

    def __unicode__(self):
        return "{machine} - {git_branch} : {last_updated}".format(
            machine=self.machine,
            git_branch=self.git_branch,
            last_updated=self.last_updated.strftime("%Y-%m-%d %H:%M:%S"))

    class Meta:
        verbose_name_plural = 'Machine Statuses'


class MachineReservation(models.Model):
    """
        Notes on the machine as well as who has it checked out.
    """
    machine = models.OneToOneField('Machine',
                                   related_name='reservation')

    notes = models.TextField(
        help_text='Reason you are checking out the server.')
    user = models.ForeignKey(
        User,
        help_text='Who checked out the server?')

    def __unicode__(self):
        return u'Reservation for {}  on {}'.format(
            self.user,
            self.machine)

    class Meta:
        verbose_name = 'Machine Reservation'
        verbose_name_plural = 'Machine Reservations'
