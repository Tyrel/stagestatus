from rest_framework import serializers

from machines.models import Machine, MachineReservation, MachineStatus


class MachineReservationSerializer(serializers.ModelSerializer):

    class Meta:
        model = MachineReservation
        fields = (
            'machine',
            'user',
            'notes',
        )


class MachineStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = MachineStatus
        fields = (
            'machine',
            'git_branch',
            'git_version',
            'last_db_refresh',
            'last_updated'
        )


class MachineSerializer(serializers.ModelSerializer):
    machine_reservation = MachineReservationSerializer(read_only=True)
    machine_status = MachineStatusSerializer(read_only=True)

    class Meta:
        model = Machine
        fields = (
            'hostname',
            'db_server',
            'machine_reservation',
            'machine_status',
        )
