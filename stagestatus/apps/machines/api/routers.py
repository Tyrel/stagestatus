import machines.api.viewsets as vs
from rest_framework import routers
router = routers.SimpleRouter()

router.register('machine', vs.MachineViewSet)
router.register('reservation', vs.MachineReservationViewSet)
router.register('status', vs.MachineStatusViewSet)
