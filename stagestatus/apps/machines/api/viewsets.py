from rest_framework import mixins
from rest_framework import viewsets

from machines.api.serializers import (
    MachineSerializer,
    MachineReservationSerializer,
    MachineStatusSerializer
)

from machines.models import (
    Machine,
    MachineReservation,
    MachineStatus
)


class MachineViewSet(mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    serializer_class = MachineSerializer
    queryset = Machine.objects.all()


class MachineReservationViewSet(mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    serializer_class = MachineReservationSerializer
    queryset = MachineReservation.objects.all()


class MachineStatusViewSet(mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    serializer_class = MachineStatusSerializer
    queryset = MachineStatus.objects.all()
