from rest_framework import serializers

from machines.models import Machine, MachineReservation, MachineStatus


class MachineReservationSerializer(serializers.ModelSerializer):

    class Meta:
        model = MachineReservation
        fields = (
            'user',
            'notes',
        )


class MachineStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = MachineStatus
        fields = (
            'git_branch',
            'git_version',
            'last_db_refresh',
            'last_updated'
        )


class MachineSerializer(serializers.ModelSerializer):
    is_reserved = serializers.SerializerMethodField()
    status = MachineStatusSerializer()
    reservation = MachineReservationSerializer()

    def get_is_reserved(self, machine):
        return machine.is_reserved()

    class Meta:
        model = Machine
        fields = (
            'hostname',
            'db_server',
            'reservation',
            'status',
            'is_reserved'
        )
