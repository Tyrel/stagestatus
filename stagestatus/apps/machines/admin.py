from django.contrib import admin

# Register your models here.
from .models import Machine, MachineStatus, MachineReservation


class MachineAdmin(admin.ModelAdmin):
    list_display = ('hostname', 'db_server')


class MachineStatusAdmin(admin.ModelAdmin):
    list_display = ('machine', 'last_db_refresh',
                    'last_updated', 'git_branch', 'git_version')


class MachineReservationAdmin(admin.ModelAdmin):
    list_display = ('machine', 'notes', 'user')


admin.site.register(Machine, MachineAdmin)
admin.site.register(MachineStatus, MachineStatusAdmin)
admin.site.register(MachineReservation, MachineReservationAdmin)
